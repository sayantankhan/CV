cv:
	latexmk -pdf CV.tex 

clean:
	find ./ -path "**/build" -print0 | xargs -0 rm -rf
	find ./ -path "**/auto" -print0 | xargs -0 rm -rf
	rm CV.pdf
